@bingo @numbers
Feature: Calling bingo numbers correctly
  As a VP of Gaming
  I want my game to call out Bingo numbers
  So that people can play with their cards

  Scenario: Check number called is between specified range
    Given I have a Bingo caller
    When I call a number
    Then the number is between 1 and 75 inclusive

  Scenario: Check all numbers are uniques and present
    Given I have a Bingo caller
    When I call a number 75 times
    Then all numbers between 1 and 75 are present
    And no number has been called more than once
