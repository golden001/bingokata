@bingo @cards
Feature: Checking bingo cards correctly
  As a VP of Gaming
  I want my game to check player's cards when they call Bingo
  So that a winner can be decided

  Background: Start new game
    Given a new game is started

  Scenario: Check player has won
    Given a player calls Bingo after all numbers on their card have been called
    When I check the card
    Then the player is the winner

  Scenario: Check player has not won
    Given a player calls Bingo before all numbers on their card have been called
    When I check the card
    Then the player is not the winner
