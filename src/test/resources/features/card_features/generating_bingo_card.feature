@bingo @cards
Feature: Generating bingo cards correctly
  As a VP of Gaming
  I want my game to generate random Bingo cards
  So that people can play

  Background: Start new game
    Given a new game is started

  Scenario: Generate a card correctly
    Given I have a Bingo card generator
    When I generate a Bingo card
    Then the generated card has 25 unique spaces
    And the generated card has 1 FREE space in the middle

  Scenario Outline: Check each column range is correct
    Given I have a Bingo card generator
    When I generate a Bingo card
    Then column '<columnChar>' only contains numbers between <minBound> and <maxBound> inclusive
    Examples:
      | columnChar | minBound | maxBound |
      | b          | 1        | 15       |
      | i          | 16       | 30       |
      | n          | 31       | 45       |
      | g          | 46       | 60       |
      | o          | 61       | 75       |
