package broker.genius.stepDefinitions

import broker.genius.game.GameManager
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class NumberSteps {

    private var game: GameManager? = null
    private var calledNumbers = mutableListOf<Int>()
    private var singleCalledNumber: Int = 0

    @Given("^I have a Bingo caller$")
    fun havingBingoCaller() {
        game = GameManager()
        assertNotNull(game!!.caller)
    }

    @When("^I call a number$")
    fun callNumber(): Int {
        singleCalledNumber = game!!.callNumber()
        return singleCalledNumber
    }

    @Then("^the number is between (\\d+) and (\\d+) inclusive$")
    fun checkNumberIsInRange(minRange: Int, maxRange: Int) {
        assertTrue { IntRange(minRange, maxRange).contains(singleCalledNumber) }
    }

    @When("^I call a number (\\d+) times$")
    fun callNumberTimes(times: Int) {
        for (i in 1..75) {
            calledNumbers.add(callNumber())
        }
    }

    @Then("^all numbers between (\\d+) and (\\d+) are present$")
    fun checkAllNumbersArePresent(minRange: Int, maxRange: Int) {
        val results: MutableList<Boolean> = mutableListOf()
        for (i in minRange..maxRange) {
            results.add(calledNumbers.contains(i))
        }
        assertTrue { !results.contains(false) }
    }

    @Then("^no number has been called more than once$")
    fun checkAllNumbersAreUnique() {
        assertTrue { calledNumbers.distinct().size == calledNumbers.size }
    }
}
