package broker.genius.stepDefinitions

import broker.genius.caller.Caller
import broker.genius.cardManager.Card.Card
import broker.genius.cardManager.CardManager
import broker.genius.game.GameManager
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import kotlin.test.*

class CardSteps {
    private var cardManager: CardManager? = null
    private var caller: Caller? = null
    private var singleGeneratedCard: Card? = null
    private var isCardWinner = false
    private val middleColumn = "n"
    private val middleRow = 3

    @Given("^a new game is started$")
    fun startNewGame() {
        val gameManager = GameManager()
        cardManager = gameManager.cardManager
        caller = gameManager.caller
    }

    @Given("^I have a Bingo card generator$")
    fun havingCardGenerator() {
        assertNotNull(cardManager)
        assertNotNull(caller)
    }

    @When("^I generate a Bingo card$")
    fun generateCard() {
        singleGeneratedCard = cardManager!!.generateCard()
    }

    @Then("^the generated card has (\\d+) unique spaces$")
    fun checkNumberAndUniques(numberOfSpaces: Int) {
        assertTrue { singleGeneratedCard!!.cells.size == 25 }
        assertEquals(numberOfSpaces, singleGeneratedCard!!.cells.distinct().size)
    }

    @Then("^column '(.*)' only contains numbers between (.*) and (.*) inclusive$")
    fun checkColumnContentRange(columnChar: String, minBound: Int, maxBound: Int) {
        assertTrue {
            singleGeneratedCard!!.checkColumnRange(columnChar, IntRange(minBound, maxBound))
        }
    }

    @Then("^the generated card has 1 FREE space in the middle$")
    fun checkMiddleSpaceIsEmpty() {
        val cellFound = singleGeneratedCard!!.getCellByColumnAndRow(middleColumn, middleRow)
        assertNotNull(cellFound, "Cell is found correctly")
        assertNull(cellFound!!.value, "Cell value has no value")
    }

    @Given("^a player calls Bingo after all numbers on their card have been called$")
    fun callBingoOnTime() {
        singleGeneratedCard = cardManager!!.generateCard()

        while (!singleGeneratedCard!!.isAllMarked()) {
            val calledNumber = caller!!.call()
            singleGeneratedCard!!.markCell(calledNumber)
        }
    }

    @Given("^a player calls Bingo before all numbers on their card have been called$")
    fun callBingoBefore() {
        singleGeneratedCard = cardManager!!.generateCard()

        for (i in 1..23) {
            singleGeneratedCard!!.markCell(caller!!.call())
        }
    }

    @When("^I check the card$")
    fun checkCards() {
        isCardWinner = singleGeneratedCard!!.isAllMarked()
    }

    @Then("^the player is the winner$")
    fun checkWinnerCards() {
        assertTrue { isCardWinner }
    }

    @Then("^the player is not the winner$")
    fun checkLooserCard() {
        assertFalse(isCardWinner)
    }
}
