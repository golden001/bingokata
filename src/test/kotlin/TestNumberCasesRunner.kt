import cucumber.api.CucumberOptions
import net.serenitybdd.cucumber.CucumberWithSerenity
import org.junit.runner.RunWith

@RunWith(CucumberWithSerenity::class)
@CucumberOptions(
        features = ["src/test/resources/features/number_features"],
        glue = ["broker.genius.stepDefinitions"],
        plugin = [
            "pretty",
            "html:target/cucumber-reports/cucumber-pretty",
            "json:target/cucumber-reports/CucumberTestReport.json",
            "rerun:target/cucumber-reports/rerun.txt"
        ],
        tags = ["@bingo", "@numbers"]
)

class TestNumberCasesRunner
