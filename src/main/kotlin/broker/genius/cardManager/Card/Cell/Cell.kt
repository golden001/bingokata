package broker.genius.cardManager.Card.Cell

class Cell(val columnChar: String, val rowNumber: Int, val value: Int? = null) {

    var marked: Boolean = false

    fun mark() {
        marked = true
    }
}
