package broker.genius.cardManager.Card

import broker.genius.cardManager.Card.Cell.Cell
import java.util.*

class Card {
    val cells = mutableListOf<Cell>()

    private var columnNames = listOf("b", "i", "n", "g", "o")
    private var rowNumbers = listOf(1, 2, 3, 4, 5)

    fun generate() {
        for (columnName in columnNames) {
            val possibleValues = getRange(columnName).toMutableList()
            for (rowNumber in rowNumbers) {
                cells.add(
                        if (columnName == "n" && rowNumber == 3) {
                            Cell(columnName, rowNumber)
                        } else {
                            val value = possibleValues[Random().nextInt(possibleValues.size)]
                            possibleValues.remove(value)
                            Cell(columnName, rowNumber, value)
                        }
                )
            }
        }
    }

    fun checkColumnRange(columnChar: String, intRange: IntRange): Boolean {
        val results = mutableListOf<Boolean>()

        for (cell in getCellsByColumnChar(columnChar)) {
            if (!isMiddleCell(cell)) {
                results.add(intRange.contains(cell.value))
            }
        }

        return !results.contains(false)
    }

    fun getCellByColumnAndRow(columnChar: String, rowNumber: Int): Cell? {
        var foundCells: Cell? = null

        for (cell in cells) {
            if (cell.columnChar == columnChar && cell.rowNumber == rowNumber) {
                foundCells = cell
            }
        }

        return foundCells
    }

    fun markCell(value: Int) {
        for (cell in cells) {
            if (!isMiddleCell(cell) && cell.value == value) {
                cell.mark()
            }
        }
    }

    fun isAllMarked(): Boolean {
        val results = mutableListOf<Boolean>()

        for (cell in cells) {
            if (!isMiddleCell(cell)) {
                results.add(cell.marked)
            }
        }

        return !results.contains(false)
    }

    fun getCellsByColumnChar(columnChar: String): MutableList<Cell> {
        val foundCells = mutableListOf<Cell>()

        for (cell in cells) {
            if (cell.columnChar == columnChar) {
                foundCells.add(cell)
            }
        }

        return foundCells
    }

    private fun isMiddleCell(cell: Cell): Boolean {
        return (cell.columnChar == "n" && cell.rowNumber == 3)
    }

    private fun getRange(columnChar: String): IntRange {
        return when (columnChar) {
            "b" -> IntRange(1, 15)
            "i" -> IntRange(16, 30)
            "n" -> IntRange(31, 45)
            "g" -> IntRange(46, 60)
            "o" -> IntRange(61, 75)
            else -> throw Exception("Invalid column char")
        }
    }
}
