package broker.genius.cardManager

import broker.genius.cardManager.Card.Card

class CardManager {

    val generatedCards = mutableListOf<Card>()

    fun generateCard(): Card {
        val newCard = Card()

        newCard.generate()
        generatedCards.add(newCard)

        return newCard
    }
}
