package broker.genius.caller

import java.util.*

class Caller(minRange: Int, maxRange: Int) {

    val calledNumbers = mutableListOf<Int>()

    private val numbers = IntRange(minRange, maxRange).toMutableList()

    fun call(): Int {
        if (numbers.size > 0) {
            val randomNumber = numbers[Random().nextInt(numbers.size)]
            calledNumbers.add(randomNumber)
            numbers.remove(randomNumber)
            return randomNumber
        } else throw Exception("No more numbers available")
    }
}
