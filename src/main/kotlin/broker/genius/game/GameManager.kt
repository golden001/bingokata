package broker.genius.game

import broker.genius.caller.Caller
import broker.genius.cardManager.CardManager

class GameManager {
    var caller: Caller? = null
    var cardManager: CardManager? = null

    init {
        caller = Caller(1, 75)
        cardManager = CardManager()
    }

    fun callNumber(): Int {
        return caller!!.call()
    }


}
