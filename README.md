# Bingo Kata
## Broker Genius
______

### Description: 
This repository was created for running a technical test for **Broker Genius**

So the intention is to build some test cases based in next paragraphs:

```
Bingo is a game of chance played using cards with numbers printed on them, which are marked off
of the card as the caller reads out some manner of random numbers. Play usually ceases once a 
certain pattern is achieved on a card, where the winner will shout out "Bingo!" in order to let the 
caller and the room know that there may be a winner.
   
There are lots of different variations of Bingo, each with their own specific rules. Classic US 
Bingo is perhaps the most well known, where the game is played using a 5x5 grid of numbers 
between 1 and 75, with a FREE space in the middle. There is also a UK version of Bingo, which uses
a 9x3 grid of spaces containing numbers between 1 and 90, of which five spaces on each row are 
filled in.
```


_____

### Pre-conditions:

```
To get the best reach for our Bingo game, we are going to model it on the US version to begin with. 
To make this work, we are going to need to be able to call out numbers, generate Bingo cards for 
people to play with, and check their cards when someone calls Bingo. Once we have got these basics 
in place, we can then start to add new features or tweak the way it works.
```
_____

### Test Cases

**Test 1**
```
As a VP of Gaming
I want my game to call out Bingo numbers
So that people can play with their cards
```

**Test 2**
```
As a VP of Gaming
I want my game to generate random Bingo cards
So that people can play
```

**Test 3**
```
As a VP of Gaming
I want my game to check player's cards when they call Bingo
So that a winner can be decided
```

For having more info about the content of every test case go [here](https://agilekatas.co.uk/katas/Bingo-Kata)

## How to Run

### Pre-conditions
- [Java SE Development Kit v8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org/install.html)

### Command

**Run all tests**

`mvn clean install`

**Run tests by tags**

`mvn clean install  -Dcucumber.options="--tags @cards"`